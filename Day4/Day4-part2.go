package main

import "fmt"

const min, max = 246515, 739105

// splits the integer into each digit and into an array
func splitDigits(i int) []int {
	var digits []int
	for i > 0 {
		d := i % 10
		digits = append([]int{d}, digits...)
		i /= 10
	}
	return digits
}

func checkTwoDigits(digits []int) bool {
	var hasRepeat bool

	for i := 0; i < len(digits)-1; i++ {
		var duplicates []int
		for j := i + 1; j < len(digits); j++ {
			if digits[i] != digits[j] {
				break
			}
			// found duplicate, group them
			duplicates = append(duplicates, digits[i], digits[j])
			// jump forward another element in array
			i++
		}
		if len(duplicates) == 2 {
			hasRepeat = true
		}
	}
	return hasRepeat
}

// ensures digits do not increase
func checkDigits(i int) bool {
	digits := splitDigits(i)
	var flag2 bool
	for ind, d := range digits {
		if ind != len(digits)-1 {
			if d > digits[ind+1] {
				flag2 = false
				break
			} else if checkTwoDigits(digits) {
				flag2 = true
			} else {
				flag2 = false
			}
		}
	}
	return flag2
}

func main() {
	var counter int

	for i := min; i <= max; i++ {
		if checkDigits(i) {
			counter++
		}
	}
	fmt.Printf("Number of different password combos are: %v\n", counter)
}
