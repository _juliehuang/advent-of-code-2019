package main

import "fmt"

const min, max = 246515, 739105

// check digits to make sure they don't increase and there
// exists adjacent digits that are the same
func checkDigits(i int) bool {
	var digits []int
	var flag bool
	for i > 0 {
		d := i % 10
		digits = append([]int{d}, digits...)
		i /= 10
	}
	for i, d := range digits {
		if i != len(digits)-1 {
			if d > digits[i+1] {
				flag = false
				break
			} else if d == digits[i+1] {
				flag = true
			}
		}
	}
	return flag
}

func main() {
	var counter int
	for i := min; i <= max; i++ {
		if checkDigits(i) {
			counter++
		}
	}
	fmt.Printf("Number of different combinations of passwords are: %v\n", counter)
}
