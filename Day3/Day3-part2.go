package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

type Point struct {
	X, Y int
}

func convertString(i string) []string {
	splitString := strings.Split(i, ",")
	return splitString
}

func setPoint(s string, p Point) []Point {
	var path []Point
	dir := string(s[0])
	steps, _ := strconv.Atoi(s[1:len(s)])
	for i := 0; i < steps; i++ {
		switch {
		case dir == "R":
			p.X = p.X + 1
		case dir == "L":
			p.X = p.X - 1
		case dir == "D":
			p.Y = p.Y - 1
		case dir == "U":
			p.Y = p.Y + 1
		}
		path = append(path, p)

	}
	return path

}

func getPath(input string) []Point {
	var path []Point
	central := Point{0, 0}
	w1 := convertString(input)
	for _, w := range w1 {
		positions := setPoint(w, central)
		path = append(path, positions...)
		central = positions[len(positions)-1]
	}
	return path
}

func findIntersection(path1, path2 []Point) []Point {
	var same []Point
	for _, p1 := range path1 {
		for _, p2 := range path2 {
			if p1 == p2 {
				same = append(same, p1)
			}
		}
	}
	return same
}

func calcManDist(intersection []Point) float64 {
	var dist []float64
	for _, point := range intersection {
		d := math.Abs(float64(point.X)) + math.Abs(float64(point.Y))
		dist = append(dist, d)
	}

	min := dist[0]
	for _, v := range dist {
		if v < min {
			min = v
		}
	}
	// fmt.Print(min)
	return min

}

func getSubPath(path []Point, intersect Point) []Point {

	result := []Point{}
	for i, v := range path {
		if v == intersect {
			result = path[:i+1]
		}
	}
	return result

}

func calcSteps(path []Point) int {
	var x_steps float64
	var y_steps float64

	for i, v := range path {
		if i != len(path)-1 {
			x_steps = x_steps + math.Abs(float64(v.X)-float64(path[i+1].X))
			y_steps = y_steps + math.Abs(float64(v.Y)-float64(path[i+1].Y))

		} else {
			break
		}
	}
	return int(x_steps + y_steps + 1)
}

func main() {
	// read-in input
	var lines []string
	var input1, input2 string

	file, err := os.Open("./Day3/input.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}
		lines = append(lines, string(line))
	}

	for i, s := range lines {
		if i == 0 {
			input1 = s
		} else {
			input2 = s
		}
	}

	path1 := getPath(input1)
	path2 := getPath(input2)
	intersection := findIntersection(path1, path2)

	min_steps := 0
	for i, v := range intersection {
		sub_path1 := getSubPath(path1, v)
		sub_path2 := getSubPath(path2, v)

		total_steps := calcSteps(sub_path1) + calcSteps(sub_path2)
		if i == 0 {

			min_steps = total_steps
		} else {
			if min_steps > total_steps {
				min_steps = total_steps
			}
		}
	}
	fmt.Printf("The fewest combined steps the wires must take to reach an intersection is %d\n.", min_steps)

}
