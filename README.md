# Advent of Code 2019

This repository contains the solutions to the 2019 [Advent of Code](https://adventofcode.com/) puzzles in Golang. 